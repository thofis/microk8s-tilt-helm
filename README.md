Voraussetzungen dafür:

- microk8s als lokales k8s-cluser
- kubectl installiert und verwendet microk8s als cluser
- beides kann mit snap installiert werden

siehe auch 
https://docs.tilt.dev/install.html und 
https://microk8s.io/docs/
https://ubuntu.com/tutorials/install-a-local-kubernetes-with-microk8s#1-overview

tilt installation anhand:
https://microk8s.io/docs/
Java-Tests mit:
https://docs.tilt.dev/example_java.html

Hilfreich:
- zsh-extensions für kubectl, microk8s, helm

Eigenes Spring-Boot-Beispiel:
https://spring.io/guides/gs/spring-boot-kubernetes/
- interessant: Dockerfile mit multistage-build um fat-jar auf mehrere Layer aufzuteilen

außerdem dashboard installiert und session-timeout deaktiviert:
https://blinkeye.github.io/post/public/2019-05-30-kubernetes-dashboard/

helm-beispiel:
sudo snap install helm --classic

https://tech.paulcz.net/blog/spring-into-kubernetes-part-2/

helm create spring-boot-helm 
helm install spring-boot-helm .
-> test im browser möglich: default helm zeigt nginx-welcome-page
helm upgrade spring-boot-helm .  

https://github.com/windmilleng/tilt.build/blob/master/docs/helm.md

https://docs.tilt.dev/api.html#api.docker_build


